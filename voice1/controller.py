from flask import Flask, render_template, request
import os
from twilio.rest import Client
from twilio.twiml.voice_response import VoiceResponse

#Globals. Need to move these into Environment variables.
ACCOUNT_SID = "ACd9e1a5d13831a104d01756aac805805b"
AUTH_TOKEN = "8d251eef6ffad2ac06f3f22cde7ae731"


app = Flask(__name__)

#Default page
@app.route('/', methods=['GET'])
def landing():
    return render_template('index.html')

# SMS
@app.route('/twilio/sms', methods=['POST'])
def sms():
    to_number = request.form['telephone']
    if (to_number == ""): to_number = "+61401277115"

    message = request.form['message']
    if (message == ""): message="No message entered"

    client = Client(ACCOUNT_SID, AUTH_TOKEN)

    client.messages.create(to=to_number, from_="+61437762499", body=message)

    return render_template('result.html', sms_number= str(to_number) )

# Voice
@app.route('/twilio/voice', methods=['GET', 'POST'])
def hello_monkey():

    to_number = request.form['telephone']
    if (to_number == ""): to_number = "+61401277115"

    message = request.form['message']
    if (message == ""): message="No message entered"


    #"""Respond to incoming requests."""
    resp = VoiceResponse()
    resp.say(message)

    return str(resp)


if __name__ == '__main__':
    app.run()
